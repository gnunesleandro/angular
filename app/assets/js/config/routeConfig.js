angular.module('marvelSPA').config(function ($routeProvider) {
	$routeProvider
	.when('/listing', {
		templateUrl : 'views/list.html'
	}).otherwise({redirectTo: '/listing'});
});
