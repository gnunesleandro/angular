angular.module('marvelSPA').factory("marvelAPI", function ($http) {
	var _getMarvelAPI = function () {
		return $http.get('http://gateway.marvel.com/v1/public/comics?ts=1&apikey=d8b3bb7e56bea2611fa279d96906b51e&hash=5d78209f281aacc0ba422a2250ee92bd');
	};
	return {
		getMarvelAPI: _getMarvelAPI
	};
});
