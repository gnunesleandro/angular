angular.module('marvelSPA').controller('marvelController', function ($scope, $http, marvelAPI) {
    $scope.marvel = function () {
        marvelAPI.getMarvelAPI().then(function success(response) {
            $scope.attribution = response.data.attributionText;
            $scope.results = response.data.data.results;
            $scope.resultValidThumbnailPath = (function () {
                var itens = [];
                for (var i = 0; i < $scope.results.length; i++) {
                    var resultsPath = $scope.results[i].thumbnail.path;
                    if (resultsPath.indexOf('image_not_available') === -1) {
                        itens.push(resultsPath);
                    }
                }
                return itens;
            })();
        },
        function myError(response) {
            console.log('Error in "MARVEL SPA"!');
        });
    };
    $scope.marvel();
});
