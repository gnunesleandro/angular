# AngularJS #

Modelo de projeto

### Requisitos ###

* [Gulp](http://gulpjs.com/)
* [Bower](https://bower.io/)

### Árvore de diretórios ###

```sh
projeto/
| - app/
| - | - assets/
| - | - | - fonts/
| - | - | - images/
| - | - | - js/
| - | - | - scss/
| - | - includes/
| - | - | - footer.html
| - | - | - header.html
| - | - views/
| - | - index.html
| - bower.json
| - gulpfile.js
| - package.json
```

### Instalação ###

* Instalar dependências

```sh
$ npm install && bower install
```

* Subir servidor de desenvolvimento

```sh
$ gulp dev
```

* Subir servidor de produção

```sh
$ gulp prod
```

### Comandos ( Extra ) ###

* Reinstalar dependências

```sh
$ gulp clean
```
