// DECLARAÇÃO DE DEPENDENCIAS


var browsersync  = require('browser-sync').create(),
    gulp         = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    browserify   = require('gulp-browserify'),
    cleancss     = require('gulp-clean-css'),
    concat       = require('gulp-concat'),
    htmlinclude  = require('gulp-html-tag-include'),
    htmlmin      = require('gulp-htmlmin'),
    imagemin     = require('gulp-imagemin');
    inject       = require('gulp-inject'),
    sass         = require('gulp-sass'),
    shell        = require('gulp-shell'),
    sourcemaps   = require('gulp-sourcemaps'),
    uglify       = require('gulp-uglify'),
    jshint       = require('gulp-jshint');


// TAREFAS DO AMBIENTE DE DESENVOLVIMENTO


// TAREFAS PARA COPIAR TODAS AS FONTES DO PROJETO
gulp.task('copy-fonts', function () {
    'use strict';
    return gulp.src([
               './app/assets/fonts/**/*',
               './bower_components/bootstrap/dist/fonts/**/*'
           ])
               .pipe(gulp.dest('./dist/assets/fonts'));
});


// TAREFAS PARA COPIAR TODAS AS IMAGES DO PROJETO
gulp.task('copy-images', function () {
    'use strict';
    return gulp.src('./app/assets/images/**/*')
               .pipe(gulp.dest('./dist/assets/images'));
});


// TAREFA PARA COPIAR EM UM UNICO ARQUIVO TODOS OS ESTILOS EXTERNOS
gulp.task('copy-vendor-css', function () {
    'use strict';
    return gulp.src([
        './bower_components/bootstrap/dist/css/bootstrap-theme.css',
        './bower_components/bootstrap/dist/css/bootstrap.css'
    ])
               .pipe(concat('vendor.css'))
               .pipe(gulp.dest('./dist/assets/css'));
});


// TAREFA PARA COPIAR EM UM UNICO ARQUIVO TODOS OS SCRIPTS EXTERNOS
gulp.task('copy-vendor-js', function () {
    'use strict';
    return gulp.src([
        './bower_components/jquery/dist/jquery.js',
        './bower_components/angular/angular.js',
        './bower_components/angular-route/angular-route.js',
        './bower_components/bootstrap/dist/js/bootstrap.js'
    ])
                .pipe(concat('vendor.js'))
                .pipe(gulp.dest('./dist/assets/js'));
});


// TAREFA PARA TRANSFORMAR TODOS ESTILOS SASS EM CSS
gulp.task('sass', function () {
    'use strict';
    return gulp.src('./app/assets/scss/*.scss')
               .pipe(sourcemaps.init())
               .pipe(sourcemaps.write({includeContent: false, sourceRoot: './maps'}))
               .pipe(sourcemaps.init({loadMaps: true}))
               .pipe(sass().on('error', sass.logError))
               .pipe(autoprefixer({browsers: ['last 2 versions'], cascade: false}))
               .pipe(sourcemaps.write('./maps'))
               .pipe(gulp.dest('./dist/assets/css'))
               .pipe(browsersync.stream());
});


// TAREFA PARA TRATAR TODOS OS SCRIPTS
gulp.task('js', function () {
    'use strict';
    return gulp.src('./app/assets/js/**/*.js')
               .pipe(jshint())
               .pipe(jshint.reporter('default'))
               .pipe(browserify())
               .pipe(gulp.dest('./dist/assets/js'));
});


// TAREFA PARA TRATAR O LIVE RELOAD APOS ALTERACAO DE UM SCRIPT
gulp.task('js-watch', ['js'], function (done) {
    'use strict';
    browsersync.reload();
    done();
});


// TAREFA PARA TRATAR INCLUDES ( HEADER E FOOTER )
gulp.task('html-include', function () {
    'use strict';
    return gulp.src('./app/**/*.html')
               .pipe(htmlinclude())
               .pipe(gulp.dest('./dist'));
});


// TAREFA PARA TRATAR AS CHAMADAS DOS ESTILOS E SCRIPTS
gulp.task('assets-inject-dev', function () {
    'use strict';
    return gulp.src('./dist/*.html')
               .pipe(inject(gulp.src([
                   './dist/assets/css/vendor.css',
                   './dist/assets/css/*.css',
                   './dist/assets/js/vendor.js',
                   './dist/assets/js/*.js',
                   './dist/assets/js/config/*.js',
                   './dist/assets/js/services/*.js',
                   './dist/assets/js/controllers/*.js',], {read: false}), {relative: true}))
                .pipe(gulp.dest('./dist'))
                .pipe(browsersync.stream());
});


// TAREFA PARA TRATAR O LIVE RELOAD DE ACORDO COM AS ALTERACOES NOS ARQUIVOS DO AMBIENTE DE DESENVOLVIMENTO
gulp.task('serve-dev', function () {
    'use strict';
    browsersync.init({
        server: './dist'
    });
    gulp.watch('./app/assets/images/**/*', ['copy-images']);
    gulp.watch('./app/assets/scss/**/*.scss', ['sass']);
    gulp.watch('./app/assets/js/**/*.js', ['js-watch']);
    gulp.watch('./app/**/*.html', ['html-include']);
    gulp.watch('./dist/**/*.html', ['assets-inject-dev']);
});


// TAREFAS DO AMBIENTE DE PRODUCAO


// TAREFA PARA MINIFICAR TODOS OS ESTILOS
gulp.task('css-minify', function () {
    'use strict';
    return gulp.src([
        './dist/assets/css/vendor.css',
        './dist/assets/css/core.css'
    ])
               .pipe(concat('core.min.css'))
               .pipe(cleancss({compatibility: 'ie8'}))
               .pipe(gulp.dest('./dist/assets/css'));
});


// TAREFA PARA MINIFICAR TODOS OS SCRIPTS
gulp.task('js-minify', function () {
    'use strict';
    return gulp.src([
        './dist/assets/js/vendor.js',
        './dist/assets/js/core.js'
    ])
              .pipe(concat('core.min.js'))
              .pipe(uglify())
              .pipe(gulp.dest('./dist/assets/js'));
});


// TAREFA PARA TRATAR AS CHAMADAS DOS ESTILOS E SCRIPTS
gulp.task('assets-inject-prod', function () {
    'use strict';
    return gulp.src('./dist/index.html')
               .pipe(inject(gulp.src([
                   './dist/assets/css/*.css',
                   './dist/assets/js/*.js'], {read: false}), {relative: true, removeTags: true}))
               .pipe(gulp.dest('./dist'));
});


// TAREFA PARA MINIFICAR O HTML
gulp.task('html-minify', function () {
    'use strict';
    return gulp.src('./dist/*.html')
               .pipe(htmlmin({collapseWhitespace: true}))
               .pipe(gulp.dest('./dist'));
});


// TAREFA PARA OTIMIZAR TODAS AS IMAGENS
gulp.task('images-minify', function () {
    'use strict';
    return gulp.src('./app/assets/images/**/*')
               .pipe(imagemin([
                   imagemin.gifsicle({interlaced: true}),
                   imagemin.jpegtran({progressive: true}),
                   imagemin.optipng({optimizationLevel: 5}),
                   imagemin.svgo({plugins: [{removeViewBox: true}]})
               ]))
               .pipe(gulp.dest('./dist/assets/images'));
});


// TAREFA PARA SUBIR O SERVIDOR COM OS ARQUIVOS DO BUILD DE PRODUCAO
gulp.task('serve-prod',  function () {
    'use strict';
    browsersync.init({
        server: './dist',
        port: '4000'
    });
});


// TAREFAS PARA TRATAR O AMBIENTE DE DESENVOLVIMENTO
gulp.task('clean', shell.task('rm -rf node_modules bower_components && npm cache clean && npm install && bower install && gulp dev'));
gulp.task('dist', shell.task('rm -rf dist'));
gulp.task('remove-other-assets', shell.task('rm -rf dist/assets/css/maps dist/assets/css/vendor.css dist/assets/css/core.css dist/assets/js/vendor.js dist/assets/js/core.js'));


// TAREFAS PARA SUBIR O SERVIDOR DE DESENVOLVIMENTO
gulp.task('copy-files', ['copy-fonts', 'copy-images', 'copy-vendor-css', 'copy-vendor-js']);
gulp.task('assets', ['sass', 'js']);
gulp.task('dev', shell.task('gulp dist && gulp copy-files && gulp assets && gulp html-include && gulp assets-inject-dev && gulp serve-dev'));


// TAREFA PARA MINIFICAR ( OTIMIZAR ) O CODIGO/PROJETO ESPELHANDO UM AMBIENTE DE PRODUCAO
gulp.task('assets-minify', ['css-minify', 'js-minify']);
gulp.task('prod', shell.task('gulp assets-minify && gulp remove-other-assets && gulp assets-inject-prod && gulp html-minify && gulp images-minify && gulp serve-prod'));


// TAREFA DE BUILD PELO JENKINS
gulp.task('build-for-dev', shell.task('gulp dist && gulp copy-files && gulp assets && gulp html-include && gulp assets-inject-dev'));
gulp.task('build-for-prod', shell.task('gulp assets-minify && gulp remove-other-assets && gulp assets-inject-prod && gulp html-minify && gulp images-minify'));
gulp.task('build', shell.task('gulp build-for-dev && gulp build-for-prod'));
